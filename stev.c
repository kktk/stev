#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define LEN(a) (sizeof(a) / sizeof(a[0]))

int fgettod(char *str, int d, int max, FILE *stream);
int fgetvar(char *str, int max, FILE *stream);
int fgetword(char *str, int max, FILE *stream);
int haspair(int c);
int isinline(int c);
int isinword(int c);
int peekc(FILE *stream);
void steverr(int ret, char *err);

enum{
	MAXVAR = UCHAR_MAX
};

int lineno = 1;

/* get chars until d into s; return string length, -1 in case of \n or EOF */
int
fgettod(char *s, int d, int n, FILE *f)
{
	int c, i;

	for(i=0; i < n-1; i++){
		c = fgetc(f);
		if(!isinline(c))
			return -1;
		else if(c == d)
			break;
		else
			s[i] = c;
	}

	s[i] = '\0';
	return i;
}

/* get var string into s; return string length or error from functions */
int
fgetvar(char *s, int n, FILE *f)
{
	int len;

	if(isinword(peekc(f)))
		len = fgetword(s, n, f);
	else if(haspair(peekc(f)))
		len = fgettod(s, haspair(fgetc(f)), n, f);
	else
		len = 0;
	return len;
}

/* get alphanumeric or '_' characters into s; return string length */
int
fgetword(char *s, int n, FILE *f)
{
	int c, i;

	for(i=0; i < n-1; i++){
		c = fgetc(f);
		if(isinword(c))
			s[i] = c;
		else{
			ungetc(c, f);
			break;
		}
	}

	s[i] = '\0';
	return i;
}

int
haspair(int c)
{
	switch(c){
	case '(': return ')';
	case '{': return '}';
	default: return 0;
	}
}

int
isinline(int c)
{
	if(c == EOF || c == '\n')
		return 0;
	else
		return 1;
}

int
isinword(int c)
{
	if(isalnum(c) || c == '_')
		return 1;
	else
		return 0;
}

int
peekc(FILE *f)
{
	int c;
	c = fgetc(f);
	ungetc(c, f);
	return c;
}

void
steverr(int r, char *s)
{
	fprintf(stderr, "stev:%d: %s\n", lineno, s);
	exit(r);
}

int
main(void)
{
	char var[MAXVAR];
	int c, vl;

	while((c = fgetc(stdin)) != EOF){
		if(c != '$')
			fputc(c, stdout);
		else if(peekc(stdin) == '$')
			fputc(fgetc(stdin), stdout);
		else if((vl = fgetvar(var, LEN(var), stdin)) < 0)
			steverr(1, "unexpected newline or EOF");
		else if(vl == 0)
			fputc(c, stdout);
		else if(getenv(var))
			fputs(getenv(var), stdout);
		if(c == '\n')
			lineno++;
	}

	return 0;
}
