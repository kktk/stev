`stev` (pronounced Steve or stév) is, first and foremost, a filter. It cannot
edit files directly, though it would be easy to do so.

It works similar to GNU's [envsubst]; `stev` reads standard input, expands
variables, and prints to standard output. By "variables" I mean strings like
$VAR and ${VAR}, like how you would use macros in `make`. To print a literal
'$', just type another '$' after it (e.g. $$).

Though `stev` does not do:

- Arithmetic Expansion
- Command Substitution
- Parameter Expansion

[envsubst]: https://gnu.org/software/gettext/manual/html_node/envsubst-Invocation.html

## Build & Install

Dependencies:

- C89 or newer compiler, libraries and headers
- a POSIX compliant system with `make`
- (Optional) scdoc for documentation/manpages

Build:

	$ make

Install:

	$ make install

There's also build-doc and install-doc targets for building and installing
documentation respectively. You can also configure some variables to your
liking in config.mk

## Examples

Examples here use POSIX shell syntax.

Expand variables in a document and print to stdout:

	$ stev <hello

Source a .cfg file and expand variables in a document:

	$ . ./config.cfg
	$ stev <before >after

Expand variables from concatenated program output:

	$ cat 01 02 03 | stev >04

Source .cfg file, expand variables from converted markdown and output to an
html file:

	$ . ./hello.cfg
	$ markdown hello.md | stev >hello.html

Source .cfg files, convert markdown to html, concatenate stdin with header and
footer, and expand variables for final output:

	$ . ./global.cfg
	$ . ./hello.cfg
	$ markdown hello.md | cat header.html - footer.html | stev > hello.html

## Contribute

I would prefer you email me (patches or otherwise) at
<kktkmailing@disroot.org>, though you could open/resolve an issue and request
for pulls using Codeberg.

You should use [git-send-email] if you can for patches, with your subjects
prefixed like this:

	[PATCH stev]: Fix incorrect behavior blabla...

Please write good commits, see [here] and [this]. Or at least follow the
style of my newer commits, see the log.

[git-send-email]: https://git-send-email.io
[here]: https://drewdevault.com/2019/02/25/Using-git-with-discipline.html
[this]: gemini://drewdevault.com/2021/01/02/2021-01-02-Every-commit-should-be-perfect.gmi

### Style

The style is mostly [aiju's] style, but also similar to [plan9front] and
[pikestyle]. I think it's a nice blend of compact yet readable.

[aiju's]: https://aiju.de/misc/c-style
[plan9front]: http://man.9front.org/6/style
[pikestyle]: http://doc.cat-v.org/bell_labs/pikestyle

## License

The code is licensed under The Unlicense, see UNLICENSE.
