.POSIX:
.SUFFIXES: .c .o .scd

include config.mk

CMD = stev
SRC = stev.c
OBJ = ${SRC:.c=.o}

DOC = stev.1.scd
MAN = ${DOC:.scd=}

all: build

build: $(CMD)

$(CMD): $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS)

.c.o:
	$(CC) -c $(CFLAGS) $<

build-doc: $(MAN)

$(MAN): $(DOC)
	for d in $?; do\
		out="$${d%.*}";\
		scdoc < $$d > $$out;\
	done

clean:
	rm -f $(CMD) $(OBJ) $(MAN)

install: $(CMD)
	mkdir -p $(PREFIX)/bin
	cp -f $(CMD) $(PREFIX)/bin
	chmod 755 $(PREFIX)/bin/$(CMD)

install-doc: $(MAN)
	for m in $?; do\
		sec="$${m##*.}";\
		mkdir -p $(PREFIX)/share/man/man$$sec;\
		cp -f $$m $(PREFIX)/share/man/man$$sec;\
		chmod 644 $(PREFIX)/share/man/man$$sec/$$m;\
	done

uninstall:
	rm -f $(PREFIX)/bin/$(CMD) $(PREFIX)/share/man/man1/stev.1
